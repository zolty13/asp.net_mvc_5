﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //public string Index()
        public ActionResult Index()
        {
            int hour = DateTime.Now.Hour;
            this.ViewBag.Greeting = hour < 17 ? "Good morning" : "Good evening";
            return this.View();
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return this.View();
        }

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse)
        {
            //TO DO wyslij do organizatora 
            if (this.ModelState.IsValid)
            {
                return this.View("Thanks", guestResponse);
            }
            else
            {
                return this.View();
            }
        }
    }
}