﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LanguageFeatures.Models;
using System.Text;
//Przypomnienie najwazniejszych cech .NET C# potrzebnych przy pracy z MVC
namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public string Index()
        {
            return "Przejście do adresu URL pokazującego przkład!";
        }

        public ViewResult CountCartPriceByExtension()
        {
            ShoppingCart cart = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product { Name = "Kajak", Price = 275.99M},
                    new Product { Name = "Kamizelka ratunkowa", Price = 49.99M},
                    new Product { Name = "Piłka nożna", Price = 19.50M},
                    new Product { Name = "Flaga narożna", Price = 34.99M},
                }
            };

            Product[] productsArray =
            {
                new Product { Name = "Kajak", Price = 275.99M},
                new Product { Name = "Kamizelka ratunkowa", Price = 49.99M},
                new Product { Name = "Piłka nożna", Price = 19.50M},
                new Product { Name = "Flaga narożna", Price = 34.99M},
            };

            decimal cartTotalPrice = cart.TotalPrices(); //MyExtensionMethods.TotalPrices(cart);
            return View("Result",
                (object)String.Format("Razem: {0:c}", cartTotalPrice));
        }
        // za pomoca  slowka YIELD
        public ViewResult FilterCartByCategory()
        {
            IEnumerable<Product> cart = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product { Name = "Kajak", Category = "Sport wodne", Price = 275.99M},
                    new Product { Name = "Kamizelka ratunkowa", Category = "Sport wodne", Price = 49.99M},
                    new Product { Name = "Piłka nożna", Category = "Piłka nożna", Price = 19.50M},
                    new Product { Name = "Flaga narożna", Category = "Piłka nożna", Price = 34.99M},
                }
            };
            decimal total = 0;
            foreach (Product prod in cart.FilterByCategory("Piłka nożna"))
            {
                total += prod.Price;
            }
            return View("Result", (object)String.Format("Razem za piłke nożna: {0}", total));
        }

        public ViewResult FilterWithDelegate()
        {
            IEnumerable<Product> cart = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product { Name = "Kajak", Category = "Sport wodne", Price = 275.99M},
                    new Product { Name = "Kamizelka ratunkowa", Category = "Sport wodne", Price = 49.99M},
                    new Product { Name = "Piłka nożna", Category = "Piłka nożna", Price = 19.50M},
                    new Product { Name = "Flaga narożna", Category = "Piłka nożna", Price = 34.99M},
                }
            };
            //Func<Product, bool> categoryFilter = delegate (Product prod)
            //{
            //    return prod.Category == "Piłka nożna";
            //};

            //Func<Product, bool> categoryFilter = prod => prod.Category == "Piłka nożna";
            decimal total = 0;
            //foreach (Product prod in cart.Filter(categoryFilter))
            foreach (Product prod in cart.Filter(prod => prod.Category == "Piłka nożna" || prod.Price > 20))
            {
                total += prod.Price;
            }
            return View("Result", (object)String.Format("Razem za piłke nożna: {0}", total));
        }
        //LINQ
        public ViewResult FindProducts()
        {
            Product[] products =
            {
                    new Product { Name = "Kajak", Category = "Sport wodne", Price = 275.99M},
                    new Product { Name = "Kamizelka ratunkowa", Category = "Sport wodne", Price = 49.99M},
                    new Product { Name = "Piłka nożna", Category = "Piłka nożna", Price = 19.50M},
                    new Product { Name = "Flaga narożna", Category = "Piłka nożna", Price = 34.99M},
            };
            //var foundProducts = from match in products
            //                    orderby match.Price descending
            //                    select new { match.Name, match.Price };
            var foundProducts = products.OrderByDescending(p => p.Price).
                Take(3).
                Select(p => new { p.Name, p.Price });
            //int count = 0;
            StringBuilder result = new StringBuilder();
            foreach (var prod in foundProducts)
            {
                result.AppendFormat("{0} Cena: {1}", prod.Name, prod.Price);
                //if (++count == 3)
                //    break;
            }
            return View("Result",
                (object)result.ToString());
        }


    }
}